# Introduction
Aurora is an *experimental* language; it is not at all built to compete with any traditional programming/scripting language as it lacks the functionality of either.
It is *NOT* meant to be used publicly, and is simply a side-project of mine.

With all that said, feel free to fork this crappy thing.

Oh, did I mention it's built on pure LuaJIT 2.1.0?
You may see some shifty things in the code, but that's simply me being too lazy to *actually* convert my source from 5.3.2 (native Lua) to 5.1 (LuaJIT).