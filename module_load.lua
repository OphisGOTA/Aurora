-- Copyright (C) 2016, Eevee Labs
-- Check bottom of file to see full MIT license.

-- FIle Name: module_load.lua
-- Brief: Loads all *Lua* modules stored in Modules table.

Modules = {
	
}

function loadFiles()
	local Errored = {}
	local Details = {}
	local Successful = {}
	for index, value in pairs(Modules) do
		local ran, errored = pcall(function()
			require(value)
		end)
		if errored then
			table.insert(Errored, "[" .. errored .. "]")
			table.insert(Details, {Name = value, Time = os.time()})
		else
			table.insert(Successful, value)
		end
	end
	return {
		getLog = function()
			local Passed = table.concat(require("src/array").reverse(Errored), "\n")
			local New, Count = "", 1
			for caught in string.gmatch(Passed, "%b[]") do
				local Name, Time = Details[Count].Name, Details[Count].Time
				Count = Count + 1
				New = New .. "[" .. Time .. "] " .. string.gmatch(caught, "%[%]") .. " [" .. Name .. "]" .. "\n"
			end
			return New
		end,
		Errors = Errored
	}
end

_load = loadFiles()
if #_load.Errors > 0 then
	print(_load.getLog())
end

--[==[

Copyright (C) 2016 Eevee Labs

Permission is hereby granted, free of charge, to any person obtaining a copy of this software -
and associated documentation files (the "Software"), to deal in the Software without restriction, including without -
limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software and -
to permit persons to whom the Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES -
OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT -
HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT -
OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

--]==]