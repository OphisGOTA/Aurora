-- Copyright (C) 2016, Eevee Labs
-- Check bottom of file to see full MIT license.

-- File Name: array.lua
-- Brief: Aurora-Lua array library for use within code.

RET = {}
error = require("src/error.lua")

function RET.getn(array)
	if not array then
		return error.throw("Missing argument!")
	end
	if not type(array) == "table" then
		return error.throw("Incorrect argument type!")
	end	
	return #array
end

function RET.getIndexName(array, index)
	if not array or not index then
		return error.throw("Missing argument!")
	end
	if not type(array) == "table" or not type(index) == "number" then
		return error.throw("Incorrect argument type!")
	end
	if not array[index] then
		return error.throw("Array index exceeds array bounds!")
	end
	local count = 0
	for name, value in pairs(array) do
		count = count + 1
		if index == count then
			return tostring(name)
		end
	end
end

function RET.getIndexNumber(array, index) --// note to self: this stops once it reaches a value
	if not array or not index then		--// breaks when two or more of same value!
		return error.throw("Missing argument!")
	end
	if not type(array) == "table" then
		return error.throw("Incorrect argument type!")
	end
	for ind, value in pairs(array) do
		if value == index then
			return value
		end
	end
end

function RET.reverse(array)
	if not array then
		return error.throw("Missing argument!")
	end
	if not type(array) == "table" then
		return error.throw("Incorrect argument type!")
	end	
	if #array == 0 then
		return {}
	end
	local new = {}
	for index = #array, 1, -1 do
		local name = RET.getIndexName(array, index)
		if tonumber(name) then
			new[tonumber(name)] = array[(#array + 1) - index]
		else
			new[name] = array[index]
		end
	end
	return new
end

function RET.copy(array, array2)
	if not array or not array2 then
		return error.throw("Missing argument(s)!")
	end
	if not type(array) == "table" or not type(array2) == "table" then
		return error.throw("Incorrect argument type(s)!")
	end
	local new = {}
	for index, value in pairs(array) do
		new[index] = value
	end
	for index, value in pairs(array2) do
		new[index] = value
	end
	return new
end

function RET.get(array, ...)
	if not array or not (...) then
		return error.throw("Missing argument(s)!")
	end
	if not type(array) == "table" then
		return error.throw("Incorrect argument #1 type!")
	end
	local args = {...}
	for index, value in pairs(args) do
		if type(value) ~= "number" then
			return error.throw("Incorrect argument #" .. tostring(index) .. " type!")
		end
		if value > #array or value < 1 then
			return error.throw("Array index exceeds or is below array bounds!")
		end
	end
	if #args == 1 then
		return array[args[1]]
	end
	if args[1] > args[2] then
		return error.throw("Argument order incorrect!")
	end
	local ret = {}
	for index = args[1], args[2] do
		table.insert(ret, array[index])
	end
	return ret
end

function RET.fget(array, index)
	if not array or not index then
		return error.throw("Missing argument(s)!")
	end
	if not type(array) == "table" or not (require("src/core_f.lua").checkt(index, "string", "number", "table")) then
		return error.throw("Incorrect argument type(s)!")
	end
	if type(index) == "string" then
		for ind, value in pairs(array) do
			if ind == index then
				return value
			end
		end
	elseif type(index) == "number" then
		return RET.get(array, index)
	elseif type(index) == "table" then
		return RET.get(array, table.unpack(index))
	end
end

function RET.remove(array, index)
	if not array or not index then
		return error.throw("Missing argument(s)!")
	end
	if not type(array) == "table" or not (require("src/core_f.lua").checkt(index, "string", "number", "table")) then
		return error.throw("Incorrect argument type(s)!")
	end
	if type(index) == "number" then
		if index > 0 and index <= #array then
			table.remove(array, index)
			return array
		else
			return error.throw("Array index exceeds array bounds!")
		end
	elseif type(index) == "string" then
		local pos = RET.getIndexNumber(array, index)
		table.remove(array, pos)
		return array
	elseif type(index) == "table" then
		for ind, value in pairs(index) do
			local pos = RET.getIndexNumber(array, value)
			local ran, errored = pcall(function()
				table.remove(array, pos)
			end)
			if errored then
				return error.throw("LUA_ERROR: " .. errored) --// in case index has already been removed
			end
		end
		return array
	end
end

function RET.insert(array, ...)
	if not array or not (...) then
		return error.throw("Missing argument(s)!")
	end
	if not type(array) == "table" then
		return error.throw("Incorrect argument #1 type!")
	end
	for index, value in pairs({...}) do
		table.insert(array, value)
	end
	return array
end

function RET.explode(array)
	if not array then
		return error.throw("Missing argument!")
	end
	if not type(array) == "table" then
		return error.throw("Incorrect argument #1 type!")
	end
	if #array == 0 then
		return require("src/data.lua").null --// not the same as nil!
	end
	local new = {}
	for index, value in pairs(array) do
		table.insert(new, value)
	end
	return table.unpack(new)
end

function RET.unpack(array)
	if not array then
		return error.throw("Missing argument!")
	end
	if not type(array) == "table" then
		return error.throw("Incorrect argument #1 type!")
	end
	if #array == 0 then
		return require("src/data.lua").null
	end
	return table.unpack(array)
end

return RET

--[==[

Copyright (C) 2016 Eevee Labs

Permission is hereby granted, free of charge, to any person obtaining a copy of this software -
and associated documentation files (the "Software"), to deal in the Software without restriction, including without -
limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software and -
to permit persons to whom the Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES -
OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT -
HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT -
OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

--]==]